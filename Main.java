import java.util.Scanner;

public class Main {

    public static void main (String[] args) {

        Scanner sc = new Scanner(System.in);

        int[] array = new int[77];
        //base cases of permutations (shows the pattern)
        array[1] = 1;
        array[2] = array[3] = 2;

        /*
         * Seeing pattern that all answers beyond the base cases are made
         * by taking the answers of the number 2 back and 3 back together,
         * assemble an array of all possible answers to reference later
         */
        for (int i = 4; i < 77; i++) {
            array[i] = array[i-2] + array[i-3];
        }

        try {
            while (true) {
                //For the number of nodes given, print the answer stored
                int n = sc.nextInt();
                System.out.println(array[n]);

            }
        }
        catch (Exception e) {

        }

    }

}